$(document).ready(function(){

	$("a").click(function(e){
		e.preventDefault();
		var colorOne = $("input.color1").val();
		var colorTwo = $("input.color2").val();
		var gradient = "linear-gradient(to right, " + colorOne + ", " + colorTwo + ")";
		$("body").css("background", gradient);
		$("p.copy").html("background: " + gradient);
	});

});